const mongoose = require('mongoose');
const { Schema } = mongoose;
const RecipientSchema = require('./recipient');
/*
NOTE: Mongo DB has a size limit for a single document (collection) of about
The maximum BSON document size is 16 megabytes.
*/
const surveySchema = new Schema({
    //NOTE: this is how we referece FK for a user in the user collection 
    _user: { type: Schema.Types.ObjectId, ref: 'users'},
    title: String,
    body: String,
    subject: String,
    recipients: [RecipientSchema],
    yes: { type: Number , default: 0},
    no: { type: Number, default: 0 },
    dateSent: Date,
    lastResponded:Date
});

mongoose.model('surveys', surveySchema);