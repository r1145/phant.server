const express = require('express');
require('dotenv').config()
const keys = require('./config/keys')
const mongoose = require('mongoose');
const cookieSession = require('cookie-session');
const passport = require('passport');
const bodyParser = require('body-parser');
require('./models/user');
require('./models/survey');
require('./services/passport');

mongoose.connect(keys.mongoURI);

const app = express();
/**Note:
 * Middelware makes some minor adjestments to the requests for instance:
 *  - Cookie middelware pulls some data out of the cookie, in this instance we are pulling the user id
 *  - Passport middleware pulls some other data such as the user id out of the cookie
 * All in all middleware is there to do some preprocessing of the incoming requests before they are sent
 * off to the diffrent route handlers, lastly they are a great location to add some logic that is common
 * to many diffrent handlers
 */

/**Note:
 * Parse incoming request bodies in a middleware before your handlers, available under the req.body property
 */
app.use(bodyParser.json());
app.use(
    cookieSession({
        //30 Days the cookie is valid
        maxAge: 30 * 24 * 60 * 60 * 1000,
        keys: [keys.cookieKey]
    })
);
app.use(passport.initialize());
app.use(passport.session());

/*  Note:
    This require returns a function with an immediate call of the app object
    so the second set of parentheses immediately invokes or calls the function
    that we just required
*/
require('./routes/authRoutes')(app);
require('./routes/billingRoutes')(app);
require('./routes/surveyRoutes')(app);

if (process.env.NODE_ENV === 'production') {
    // makes sure express will server up prod assests
    // like our main.js, or main.css files
    app.use(express.static('client/build'));

    // Express will however serve up the index.html file
    // if it doesn't recognize the route
    const path = require('path');
    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
    })
}

const PORT = process.env.PORT || 5000;
app.listen(PORT);